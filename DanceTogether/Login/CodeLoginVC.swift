//
//  CodeLoginVC.swift
//  DanceTogether
//
//  Created by Zejun Chen on 2022/4/25.
//

import UIKit
import LeanCloud


private let totalTime = 5
class CodeLoginVC: UIViewController {
    
    private var timeRemain = totalTime
    
    @IBOutlet weak var phoneNumTF: UITextField!
    @IBOutlet weak var authCodeTF: UITextField!
    @IBOutlet weak var getAuthCodeBtn: UIButton!
    @IBOutlet weak var loginBtn: UIButton!
    
    lazy var timer = Timer()
    
    private var phoneNumStr: String { phoneNumTF.unwrappedText }
    private var authCodeStr: String { authCodeTF.unwrappedText }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyBoardWhenTappedAround()
        
        loginBtn.setToDisabled()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        phoneNumTF.becomeFirstResponder()
    }
    
    @IBAction func dismiss(_ sender: Any) {
        dismiss(animated: true)
    }
    
    @IBAction func TFEditingChanged(_ sender: UITextField) {
        if sender == phoneNumTF{
            getAuthCodeBtn.isHidden = !phoneNumStr.isPhoneNum && getAuthCodeBtn.isEnabled
        }
        
        if phoneNumStr.isPhoneNum && authCodeStr.isAuthCode{
            loginBtn.setToEnabled()
        }else{
            loginBtn.setToDisabled()
        }
    }
    
    @IBAction func getAuthCode(_ sender: Any) {
        getAuthCodeBtn.isEnabled = false
        setAuthCodeBtnDisabledText()
        authCodeTF.becomeFirstResponder()
        
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(changeAuthCodeBtnText), userInfo: nil, repeats: true)
        
        let variables: LCDictionary = [ "name": LCString("DanceTogether"),"ttl": LCNumber(5)]
        
        LCSMSClient.requestShortMessage(
            mobilePhoneNumber: phoneNumStr,
            templateName: "Order_Notice",
            signatureName: "sign_BuyBuyBuy",
            variables: variables)
        { result in
            if case let .failure(error: error) = result{
                print(error.reason ?? "短信验证码未知错误")
            }
        }
    }
    
    
    @IBAction func login(_ sender: UIButton) {
        view.endEditing(true)
        
        showLoadHUD()
        
        LCUser.signUpOrLogIn(mobilePhoneNumber: phoneNumStr, verificationCode: authCodeStr) { (result) in
            switch result {
            case let .success(object: user):
                let randomNickName = "Dancer\(String.randomString(6))"
                self.configAfterLogin(user,randomNickName)
                
            case let .failure(error: error):
                self.hideLoadHUD()
                DispatchQueue.main.async {
                    self.showTextHUD("登录失败", true, error.reason)
                }
            }
        }
    }

}

extension CodeLoginVC: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let limit = textField == phoneNumTF ? 11 : 6
        
        
        let isExceed = range.location >= limit || (textField.unwrappedText.count + string.count) > limit
        if isExceed{
            showTextHUD("最多只能输入\(limit)位哦")
        }
        return !isExceed
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == phoneNumTF{
            authCodeTF.becomeFirstResponder()
        }else{
            if loginBtn.isEnabled{
                login(loginBtn)
            }
        }
        
        return true
    }
}

extension CodeLoginVC{
    @objc private func changeAuthCodeBtnText(){
        timeRemain -= 1
        setAuthCodeBtnDisabledText()
        
        if timeRemain <= 0{
            timer.invalidate()
            timeRemain = totalTime
            getAuthCodeBtn.isEnabled = true
            getAuthCodeBtn.setTitle("发送验证码", for: .normal)
            
            getAuthCodeBtn.isHidden = !phoneNumStr.isPhoneNum
        }
    }
    
}

extension CodeLoginVC{
    private func setAuthCodeBtnDisabledText(){
        getAuthCodeBtn.setTitle("重新发送(\(timeRemain)s)", for: .disabled)
    }
}

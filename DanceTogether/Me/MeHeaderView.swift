//
//  MeHeaderView.swift
//  DanceTogether
//
//  Created by Zejun Chen on 2022/5/8.
//

import UIKit
import LeanCloud
import Kingfisher

class MeHeaderView: UIView {
    
    @IBOutlet weak var rootStackView: UIStackView!

    @IBOutlet weak var backOrDrawerBtn: UIButton!
    @IBOutlet weak var shareBtn: UIButton!
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nickNameLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var introLabel: UILabel!
    @IBOutlet weak var likedAndFavedLabel: UILabel!
    @IBOutlet weak var editOrFollowBtn: UIButton!
    @IBOutlet weak var settingOrChatBtn: UIButton!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func awakeFromNib() {
        super.awakeFromNib()
        editOrFollowBtn.makeCapsule()
        settingOrChatBtn.makeCapsule()
    }
    
    var user: LCUser!{
        didSet{
            avatarImageView.kf.setImage(with: user.getImageURL(from: kAvatarCol, .avatar))
            nickNameLabel.text = user.getExactStringVal(kNickNameCol)
            let gender = user.getExactBoolValDefaultF(kGenderCol)
            genderLabel.text = gender ? "♂" : "♀"
            genderLabel.textColor = gender ?  blueColor : mainColor
            
            idLabel.text = "\(user.getExactIntVal(kIDCol))"
            
            let intro = user.getExactStringVal(kIntroCol)
            introLabel.text = user.getExactStringVal(kIntroCol).isBlank ? kIntroPH : intro
            
            guard let userObjectId = user.objectId?.stringValue else { return }
            let query = LCQuery(className:  kUserInfoTable)
            query.whereKey(kUserObjectIdCol, .equalTo(userObjectId))
            query.getFirst{ res in
                if case let .success(object: userInfo) = res{
                    let likeCount =  userInfo.getExactIntVal(kLikeCountCol)
                    let favCount =  userInfo.getExactIntVal(kFavCountCol)
                    
                    DispatchQueue.main.async {
                        self.likedAndFavedLabel.text = "\(likeCount + favCount)"
                    }
                }
            }
        }
    }
    
//    @IBAction func logouttest(_ sender: Any) {
//        LCUser.logOut()
//    }
    
}

//
//  MeVC-Header.swift
//  DanceTogether
//
//  Created by Zejun Chen on 2022/5/9.
//

import Foundation
import UIKit
import LeanCloud

extension MeVC{
    func setHeaderView() -> UIView{
        //约束
        meHeaderView.translatesAutoresizingMaskIntoConstraints = false
        meHeaderView.heightAnchor.constraint(equalToConstant: meHeaderView.rootStackView.frame.height + 26).isActive = true
        
        meHeaderView.user = user
        if isFromNote{
            meHeaderView.backOrDrawerBtn.setImage(largeIcon("chevron.left"), for: .normal)
        }
        meHeaderView.backOrDrawerBtn.addTarget(self, action: #selector(backOrDrawer), for: .touchUpInside)
        
        //是不是本人
        if isMySelf{
            meHeaderView.introLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(editIntro)))
        }else{
            if user.getExactStringVal(kIntroCol).isBlank{
                meHeaderView.introLabel.isHidden = true
            }
            //是否已登录，看有没有关注
            if let _ = LCApplication.default.currentUser{
                //判断是否关注，不做了
                meHeaderView.editOrFollowBtn.setTitle("关注", for: .normal)
                meHeaderView.editOrFollowBtn.backgroundColor = mainColor
            }else{
                meHeaderView.editOrFollowBtn.setTitle("关注", for: .normal)
                meHeaderView.editOrFollowBtn.backgroundColor = mainColor
            }
            
            meHeaderView.settingOrChatBtn.setImage(fontIcon("ellipsis.bubble", fontSize: 13), for: .normal)
        }
        
        meHeaderView.editOrFollowBtn.addTarget(self, action: #selector(editOrFollow), for: .touchUpInside)
        meHeaderView.settingOrChatBtn.addTarget(self, action: #selector(settingOrChat), for: .touchUpInside)
        
        return meHeaderView
    }
}

//
//  MeVC-Config.swift
//  DanceTogether
//
//  Created by Zejun Chen on 2022/5/9.
//

import Foundation
import LeanCloud

extension MeVC{
    func config(){
        navigationItem.backButtonDisplayMode = .minimal
        
        if let user = LCApplication.default.currentUser, user == self.user{
            isMySelf = true
        }
    }
}

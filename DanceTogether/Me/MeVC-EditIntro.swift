//
//  MeVC-EditIntro.swift
//  DanceTogether
//
//  Created by Zejun Chen on 2022/5/9.
//

import Foundation

extension MeVC{
    @objc func editIntro(){
        let vc = storyboard!.instantiateViewController(identifier: kIntroVCID) as! IntroVC
        vc.intro = user.getExactStringVal(kIntroCol)
        vc.delegate = self
        present(vc, animated: true)
    }
}

extension MeVC: IntroVCDelegate{
    func updateIntro(_ intro: String) {
        //UI
        meHeaderView.introLabel.text = intro.isBlank ? kIntroPH : intro
        //云端
        //只有是myself才能进到个人简介编辑页面，所以一定可以改自己的的user表数据
        try? user.set(kIntroCol, value: intro)
        user.save{ _ in }
    }
    
    
}

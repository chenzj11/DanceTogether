//
//  MeVC.swift
//  DanceTogether
//
//  Created by Zejun Chen on 2022/4/18.
//

import UIKit
import LeanCloud
import SegementSlide

class MeVC: SegementSlideDefaultViewController {
    
    var user: LCUser
    lazy var meHeaderView = Bundle.loadView(fromNib: "MeHeaderView", with: MeHeaderView.self)
    
    var isFromNote = false
    var isMySelf = false
    
    init?(coder: NSCoder, user: LCUser){
        self.user = user
        super.init(coder: coder)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        //去掉草稿页面的返回文字
        config()
        
        setUI()
        
    
    }
    
    override var bouncesType: BouncesType { .child }
    
    override func segementSlideHeaderView() -> UIView? {
        setHeaderView()
    }
    
    override var titlesInSwitcher: [String] {
        return ["笔记", "收藏", "赞过"]
    }
    
    override var switcherConfig: SegementSlideDefaultSwitcherConfig{
        var config = super.switcherConfig
        //居中对齐
        config.type = .tab
        config.selectedTitleColor = .label
        config.indicatorColor = mainColor
        return config
    }
    
    override func segementSlideContentViewController(at index: Int) -> SegementSlideContentScrollViewDelegate? {
        let isMyDraft = (index == 0) && (isMySelf) && (UserDefaults.standard.integer(forKey: kDraftNoteCount) > 0)
        
        
        let vc = storyboard!.instantiateViewController(identifier: kWaterfallVCID) as! WaterfallVC
        vc.isMyDraft = isMyDraft
        vc.user = user
        vc.isMyNote = index == 0
        vc.isMyFav = index == 1
        vc.isMyselfLike = (isMySelf && index == 2)
        vc.isFromMeVC = true
        vc.fromMeVCUser = user
        return vc
    }
    
    //    @IBAction func logoutTest(_ sender: Any) {
    //        LCUser.logOut()
    //
    //        let loginVC = storyboard!.instantiateViewController(identifier: kLoginVCID)
    //
    //        loginAndMeParentVC.removeChildren()
    //        loginAndMeParentVC.add(child: loginVC)
    //    }
    //
    //    @IBAction func showDraftNote(_ sender: Any) {
    //        let navi = storyboard!.instantiateViewController(identifier: kDraftNotesNaviID)
    //        navi.modalPresentationStyle = .fullScreen
    //        present(navi, animated: true)
    //    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

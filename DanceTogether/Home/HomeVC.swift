//
//  HomeVC.swift
//  DanceTogether
//
//  Created by Zejun Chen on 2022/3/14.
//

import UIKit
import XLPagerTabStrip

class HomeVC: ButtonBarPagerTabStripViewController {

    override func viewDidLoad() {
        
        // MARK: 设置上方的bar，按钮，条的UI
        
        // bar 整体在storyboard上搭建
        
        //selectedBar按钮下方的条
        settings.style.selectedBarBackgroundColor = mainColor
        settings.style.selectedBarHeight = 3
        
        settings.style.buttonBarItemBackgroundColor = .clear
        settings.style.buttonBarItemTitleColor = .label
        settings.style.buttonBarItemFont = .systemFont(ofSize: 16)
        
        super.viewDidLoad()
        
        //让containerView在划到边界的时候不会继续往下
        containerView.bounces = false
        
        //当不同item被选中的时候上面的item需要被改变
        changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }

            oldCell?.label.textColor = .secondaryLabel
            newCell?.label.textColor = .label
        }
    }
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let discoveryVC = storyboard!.instantiateViewController(withIdentifier: kDiscoveryVCID)
        let followVC = storyboard!.instantiateViewController(withIdentifier: kFollowVCID)
        let nearbyVC = storyboard!.instantiateViewController(withIdentifier: kNearbyVCID)
        return [discoveryVC, followVC, nearbyVC]
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

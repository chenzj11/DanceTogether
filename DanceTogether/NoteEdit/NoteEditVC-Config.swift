//
//  NoteEditVC-Config.swift
//  DanceTogether
//
//  Created by Zejun Chen on 2022/3/26.
//

import Foundation

extension NoteEditVC{
    func config(){
        photoCollectionView.dragInteractionEnabled = true // 开启拖放功能
        hideKeyBoardWhenTappedAround()
        titleCountLabel.text = "\(kMaxNoteTitleCount)"
        
        //保证添加正文和添加标题在最前面的地方对齐,去除文本的placeholder上下左右边距
        let lineFragmentPadding = textView.textContainer.lineFragmentPadding
        textView.textContainerInset = UIEdgeInsets(top: 0, left: -lineFragmentPadding, bottom: 0, right: lineFragmentPadding)
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 6
        let typingAttributes: [NSAttributedString.Key: Any] = [
            .paragraphStyle: paragraphStyle,
            .font: UIFont.systemFont(ofSize: 14),
            .foregroundColor: UIColor.secondaryLabel
        ]
        textView.typingAttributes = typingAttributes
        textView.tintColorDidChange()
        
        textView.inputAccessoryView = Bundle.loadView(fromNib: "TextViewIAView", with: TextViewIAView.self)
            textViewIAView.doneBtn.addTarget(self, action: #selector(resignTextView), for: .touchUpInside)
            textViewIAView.maxTextCountLabel.text = "/\(kMaxNoteTextCount)"
//        textView.inputAccessoryView
        
        locationManager.requestWhenInUseAuthorization()
        AMapLocationManager.updatePrivacyShow(.didShow, privacyInfo: .didContain)
        AMapLocationManager.updatePrivacyAgree(.didAgree)
        //AMapSearchAPI.updatePrivacyShow
        //AMapSearchAPI.updatePrivacyAgree
    }
}

extension NoteEditVC{
    @objc private func resignTextView(){
        textView.resignFirstResponder()
    }
}

//
//  PhotoFooter.swift
//  DanceTogether
//
//  Created by Zejun Chen on 2022/3/25.
//

import UIKit

class PhotoFooter: UICollectionReusableView {
    @IBOutlet weak var addPhotoBtn: UIButton!
    
    override func awakeFromNib() {//和viewdidload差不多
        super.awakeFromNib()
        addPhotoBtn.layer.borderWidth = 1
        addPhotoBtn.layer.borderColor = UIColor.quaternaryLabel.cgColor
    }
}

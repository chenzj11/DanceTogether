//
//  NoteEditVC.swift
//  DanceTogether
//
//  Created by Zejun Chen on 2022/3/25.
//

import UIKit
import YPImagePicker
import SKPhotoBrowser
import AVKit
import CoreLocation
import LeanCloud

class NoteEditVC: UIViewController {
    
    var draftNote: DraftNote?//如果是编辑草稿的话会有对应的值
    var updateDraftNoteFinished: (() -> ())?
    var postDraftNoteFinished: (() -> ())?
    
    var note: LCObject?
    var updateNoteFinished: ((String) -> ())?
    
    var photos: [UIImage] = []
//    var videoURL: URL = Bundle.main.url(forResource: "testVideo", withExtension: "mp4")!
//    var videoURL:URL? = Bundle.main.url(forResource: "TV", withExtension: "mp4")
    var videoURL:URL?
    
    var channel = ""
    var subChannel = ""
    var poiName = ""
    
    let locationManager = CLLocationManager()
    @IBOutlet weak var photoCollectionView: UICollectionView!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var titleCountLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var channelIcon: UIImageView!
    @IBOutlet weak var channelLabel: UILabel!
    @IBOutlet weak var channelPlaceholderLabel: UILabel!
    @IBOutlet weak var poiNameLabel: UILabel!
    @IBOutlet weak var poiNameIcon: UIImageView!
    
    var photoCount:Int{photos.count}
    var isVideo: Bool {videoURL != nil}
    var textViewIAView: TextViewIAView{textView.inputAccessoryView as! TextViewIAView}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
        setUI()
    }
    
    
    @IBAction func TFEditBegin(_ sender: Any) {
        titleCountLabel.isHidden = false
    }
    @IBAction func TFEditEnd(_ sender: Any) {
        titleCountLabel.isHidden = true
    }
    @IBAction func TFEndOnExit(_ sender: Any) {
    }
    @IBAction func TFEditChanged(_ sender: Any) {
        handleTFEditChanged()
    }
    
    //待做：做草稿
    @IBAction func saveDraftNote(_ sender: Any) {
        guard isValidateNote() else { return }
        
        if let draftNote = draftNote {
            updateDraftNote(draftNote)
        }else{
            createDraftNote()
        }
    }
    
    @IBAction func postNote(_ sender: Any) {
        guard isValidateNote() else { return }
        if let draftNote = draftNote {
            postDraftNote(draftNote)
        }else if let note = note{//更新笔记
            updateNote(note)
        }else{
            createNote()
        }
    }
    
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let channelVC = segue.destination as? ChannelVC{
            view.endEditing(true)
            channelVC.PVdelegate = self
        }else if let poiVC = segue.destination as? POIVC{
            poiVC.delegate = self
            poiVC.poiName = poiName
        }
    }
    
}

//extension NoteEditVC: UITextFieldDelegate{
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//
//        let isExceed = range.location >= kMaxNoteTitleCount || (textField.unwrappedText.count +  string.count) > kMaxNoteTitleCount
//
//        if isExceed{
//            showTextHUD("标题最左输入\(kMaxNoteTitleCount)个字哦！")
//        }
//
//        return !isExceed
//    }//最重要的delegate函数，用来限制输入长度。
//}


extension NoteEditVC: UITextViewDelegate{
    func textViewDidChange(_ textView: UITextView) {
        guard textView.markedTextRange == nil else { return }
        textViewIAView.currentTextCount = textView.text.count
    }
}

extension NoteEditVC: ChannelVCDelegate{
    func updateChannel(channel: String, subChannel: String) {
        self.channel = channel
        self.subChannel = subChannel
        //UI
        updateChannelUI()
    }
}

extension NoteEditVC: POIVCDelegate{
    func updatePOIName(_ poiName: String) {
        
        if poiName == kPOIsInitArr[0][0]{
            //数据
            self.poiName = ""
        }else{
            //数据
            self.poiName = poiName
        }
        //UI
        updatePOINameUI()
    }
}

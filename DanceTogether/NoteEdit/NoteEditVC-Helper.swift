//
//  NoteEditVC-Helper.swift
//  DanceTogether
//
//  Created by Zejun Chen on 2022/4/18.
//

import Foundation

extension NoteEditVC{
    func isValidateNote() -> Bool{
        guard !photos.isEmpty else{
            showTextHUD("至少需要一张图片哦")
            return false
        }
        
        guard textViewIAView.currentTextCount <= kMaxNoteTextCount else {
            showTextHUD("正文最多输入\(kMaxNoteTextCount)个字哦!")
            return false
        }
        
        return true
    }
    
    func handleTFEditChanged(){
        //如果正在输入的话就不进行判断
        guard titleTextField.markedTextRange == nil else{ return }
        if titleTextField.unwrappedText.count > kMaxNoteTitleCount{
            //如果超出长度的话截取出前缀就好
            titleTextField.text = String(titleTextField.unwrappedText.prefix(kMaxNoteTitleCount))
            showTextHUD("标题最左输入\(kMaxNoteTitleCount)个字哦！")
            DispatchQueue.main.async {
                let end = self.titleTextField.endOfDocument
                self.titleTextField.selectedTextRange = self.titleTextField.textRange(from: end, to: end)
            }
        }
        titleCountLabel.text = "\(kMaxNoteTitleCount - titleTextField.unwrappedText.count)"
    }
    
    func showAllowPushAlert(){
        UNUserNotificationCenter.current().getNotificationSettings { setting in
            switch setting.authorizationStatus{
            case .denied:
                DispatchQueue.main.async{let alert = UIAlertController(title: #""DanceTogether"想给您发送通知"#, message: "收到评论后第一时间就知道哦~", preferredStyle: .alert)
                let notAllowAction = UIAlertAction(title: "不允许", style: .cancel)
                let allowAction = UIAlertAction(title: "允许", style: .default){ _ in
                    jumpToSettings()
                }
                alert.addAction(notAllowAction)
                alert.addAction(allowAction)
                self.view.window?.rootViewController?.present(alert, animated: true)}
            default:
                break
            }
        }
    }
}

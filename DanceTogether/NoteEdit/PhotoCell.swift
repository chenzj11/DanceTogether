//
//  PhotoCell.swift
//  DanceTogether
//
//  Created by Zejun Chen on 2022/3/25.
//

import UIKit

class PhotoCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
}

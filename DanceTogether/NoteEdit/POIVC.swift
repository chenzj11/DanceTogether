//
//  POIVC.swift
//  DanceTogether
//
//  Created by Zejun Chen on 2022/3/29.
//

import UIKit

class POIVC: UIViewController {
    
    var delegate: POIVCDelegate?
    var poiName = ""
    
    let locationManager = AMapLocationManager()
    lazy var mapSearch = AMapSearchAPI()
    lazy var aroundSearchRequest: AMapPOIAroundSearchRequest = {
        let request = AMapPOIAroundSearchRequest()
        
        request.location = AMapGeoPoint.location(withLatitude: latitude, longitude: longitude)
                request.types = kPOITypes 
        request.requireExtension = true
        request.offset = kPOIsOffset
        request.requireExtension = true
        request.requireSubPOIs = true
        return request
    }()
    
    lazy var keywordsSearchRequest: AMapPOIKeywordsSearchRequest = {
        let request = AMapPOIKeywordsSearchRequest()
        request.requireExtension = true
        return request
    }()
    lazy var footer = MJRefreshAutoNormalFooter()
    
    var pois = kPOIsInitArr
    var aroundSearchPOIs = kPOIsInitArr
    
    var latitude = 0.0
    var longitude = 0.0
    var keywords = ""
    var pageCount = 1
    var currentAroundPage = 1
    var currentKeywordsPage = 1
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        config() 
        
        requestLocation()
        // Do any additional setup after loading the view.
    }
    
}



extension POIVC: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        pois.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: kPOICellID,for: indexPath) as! POICell
        
        let poi = pois[indexPath.row]
        cell.poi = poi
        
        if poi[0] == poiName{cell.accessoryType = .checkmark}
        
        return cell
    }
    
    
}

//MARK: - UITableViewDelegate
extension POIVC: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)!
        cell.accessoryType = .checkmark
        
        delegate?.updatePOIName(pois[indexPath.row][0])
        
        dismiss(animated: true)
    }
}


extension POIVC{
    func endRefreshing(_ currentPage: Int){
        if currentPage < pageCount{
            footer.endRefreshing()
        }else{
            footer.endRefreshingWithNoMoreData()
        }
    }
}

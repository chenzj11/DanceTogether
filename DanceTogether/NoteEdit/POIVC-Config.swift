//
//  POIVC-Config.swift
//  DanceTogether
//
//  Created by Zejun Chen on 2022/4/1.
//

import Foundation

extension POIVC{
    
    func config(){
        //定位
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        
        locationManager.locationTimeout = 5
        
        locationManager.reGeocodeTimeout = 5
        
        mapSearch?.delegate = self
        
        tableView.mj_footer = footer
        
        if let cancelButton = searchBar.value(forKey: "cancelButton") as? UIButton{
            cancelButton.isEnabled = true
        }
    }
    
}

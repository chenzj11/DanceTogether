//
//  MyDraftNoteWaterfallCell.swift
//  DanceTogether
//
//  Created by Zejun Chen on 2022/5/9.
//

import UIKit

class MyDraftNoteWaterfallCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var countLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        countLabel.text = "\(UserDefaults.standard.integer(forKey: kDraftNoteCount))"
    }

}

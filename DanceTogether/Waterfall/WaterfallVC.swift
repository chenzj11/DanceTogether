//
//  WaterfallVC.swift
//  DanceTogether
//
//  Created by Zejun Chen on 2022/3/21.
//

import UIKit
import CHTCollectionViewWaterfallLayout
import XLPagerTabStrip
import LeanCloud
import SegementSlide

class WaterfallVC: UICollectionViewController, SegementSlideContentScrollViewDelegate{
    
    var channel = ""
    lazy var header = MJRefreshNormalHeader()
    @objc var scrollView: UIScrollView{ collectionView }
    
    var isDraft = false
    var draftNotes: [DraftNote] = []
    
    var notes: [LCObject] = []
    
    var isMyDraft = false
    //仅用于个人页面
    var user: LCUser?
    var isMyNote = false
    var isMyFav = false
    //点赞去重，不访问云端
    var isMyselfLike = false
    
    //优化
    var isFromMeVC = false
    var fromMeVCUser: LCUser?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
        
        if let _ = user {//个人页面
            if isMyNote{
                header.setRefreshingTarget(self, refreshingAction: #selector(getMyNotes))
            }else if isMyFav{
                header.setRefreshingTarget(self, refreshingAction: #selector(getMyFavNotes))
            }else{//赞过
                header.setRefreshingTarget(self, refreshingAction: #selector(getMyLikeNotes))
            }
            header.beginRefreshing()
        }else if isDraft{
            getDraftNotes()
        }else{
            header.setRefreshingTarget(self, refreshingAction: #selector(getNotes))
            header.beginRefreshing()
        }
        
    }

    @IBAction func dismissDraftNotesVC(_ sender: Any) {
        dismiss(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - CHTCollectionViewDelegateWaterfallLayout

extension WaterfallVC: IndicatorInfoProvider{
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        IndicatorInfo(title: channel)
    }
}

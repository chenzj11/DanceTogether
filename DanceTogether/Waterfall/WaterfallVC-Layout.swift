//
//  WaterfallVC-Layout.swift
//  DanceTogether
//
//  Created by Zejun Chen on 2022/5/10.
//

import CHTCollectionViewWaterfallLayout

extension WaterfallVC: CHTCollectionViewDelegateWaterfallLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellW = (screenRect.width - kWaterfallPadding * 3) / 2
        
        var cellH: CGFloat = 0
        
        if isMyDraft, indexPath.item == 0{
            cellH = 100
        }else if isDraft{
            let draftNote = draftNotes[indexPath.item]
            let imageSize = UIImage(draftNote.coverPhoto)?.size ?? imagePH.size
            let imageH = imageSize.height
            let imageW = imageSize.width
            let imageRatio = imageH / imageW
            
            
            cellH = cellW * imageRatio + kDraftNoteWaterfallCellBottomViewH
        }else{
            //要先经过云端，所以存储图片的时候要作为一个字段上传到云端
            let offset = isMyDraft ? 1 : 0
            let note = notes[indexPath.item - offset]
            let coverPhotoRatio = CGFloat(note.getExactDoubleVal(kCoverPhotoRatioCol))
            cellH = cellW * coverPhotoRatio
            
        }
        
        return CGSize(width: cellW, height: cellH)
    }
}

//
//  WaterfallCell-Like.swift
//  DanceTogether
//
//  Created by Zejun Chen on 2022/5/3.
//

import LeanCloud

extension WaterfallCell{
    @objc func likeBtnTappedWhenLogin(){
        
        //点击偶数次直接不发送网络请求
        if likeCount != currentLikeCount{
            guard let note = note, let authorObjectId = (note.get(kAuthorCol) as? LCUser)?.objectId?.stringValue else { return }
            
            let user = LCApplication.default.currentUser!
            let offset = isLike ? 1 : -1
            currentLikeCount += offset
            if isLike{
                
                //userLike中间表
                let userLike = LCObject(className: kUserLikeTable)
                try? userLike.set(kUserCol, value: user)
                try? userLike.set(kNoteCol, value: note)
                userLike.save{ _ in }
                
                //原子操作
                try? note.increase(kLikeCountCol)
                
                //个人获得的的点赞和收藏数量，由于LC不能够修改别的用户的数据，所以没办法做
                LCObject.userInfo(where: authorObjectId, increase: kLikeCountCol)
            }else{
                let query = LCQuery(className: kUserLikeTable)
                query.whereKey(kUserCol, .equalTo(user))
                query.whereKey(kNoteCol, .equalTo(note))
                //有且只有一条
                query.getFirst{ res in
                    if case let .success(object: userLike) = res{
                        userLike.delete{ _ in }
                    }
                }
                
                try? note.set(kLikeCountCol, value: likeCount)
                note.save { _ in }
                
                LCObject.userInfo(where: authorObjectId, decrease: kLikeCountCol, to: likeCount)
                
            }
        }
    }
}

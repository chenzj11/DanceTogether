//
//  WaterfallVC-Delegate.swift
//  DanceTogether
//
//  Created by Zejun Chen on 2022/4/15.
//

import Foundation
import UIKit

extension WaterfallVC{
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if isMyDraft, indexPath.item == 0{
            let navi = storyboard!.instantiateViewController(identifier: kDraftNotesNaviID) as! UINavigationController
            navi.modalPresentationStyle = .fullScreen
            ((navi.topViewController) as! WaterfallVC).isDraft = true
            present(navi, animated: true)
        }else if isDraft{
            let draftNote = draftNotes[indexPath.item]
            
            if let photoData = draftNote.photos,
            let photoDataArr = try? JSONDecoder().decode([Data].self, from: photoData){
                
                let photos = photoDataArr.map { UIImage($0) ?? imagePH }
                
//                draftNote.video
                
                let videoURL = FileManager.default.save(draftNote.video, to: "video", as: "\(UUID().uuidString).mp4")
                
                let vc = storyboard!.instantiateViewController(identifier: kNoteEditVCID) as! NoteEditVC
                vc.draftNote = draftNote
                vc.photos = photos
                vc.videoURL = videoURL
                vc.updateDraftNoteFinished = {
                    self.getDraftNotes()
                }
                vc.postDraftNoteFinished = {
                    self.getDraftNotes()
                }
                
                navigationController?.pushViewController(vc, animated: true)
            }else{
                showTextHUD("加载草稿失败")
            }
        }else{
            //使用dependency Injection 用一个对象才能创建另外一个对象
            //如果只使用identifier，那就只会走required init
            let offset = isMyDraft ? 1 : 0
            let item = indexPath.item - offset
            let detailVC = storyboard!.instantiateViewController(identifier: kNoteDetailVCID){ coder in
                NoteDetailVC(coder: coder, note: self.notes[item])
            }
            if let cell = collectionView.cellForItem(at: indexPath) as? WaterfallCell{
                detailVC.isLikeFromWaterfallCell = cell.isLike
            }
            
            detailVC.modalPresentationStyle = .fullScreen
            detailVC.delegate = self
            
            detailVC.delNoteFinished = {
                self.notes.remove(at: item)
                collectionView.performBatchUpdates{
                    self.collectionView.deleteItems(at: [indexPath])
                }
            }
            detailVC.isFromMeVC = isFromMeVC
            detailVC.fromMeVCUser = fromMeVCUser
            detailVC.cellItem = indexPath.item
            detailVC.noteHeroID = "noteHeroID\(indexPath.item)"
            
            present(detailVC, animated: true)
        }
    }
}

extension WaterfallVC: NoteDetailVCDelegate{
    func updateLikedBtn(cellItem: Int, isLike: Bool, likeCount: Int) {
        if let cell = collectionView.cellForItem(at: IndexPath(item: cellItem, section: 0)) as? WaterfallCell{
            cell.likeBtn.isSelected = isLike
            cell.likeCount = likeCount
            cell.currentLikeCount = likeCount
        }
    }
    
}

//
//  WaterfallVC-LoadData.swift
//  DanceTogether
//
//  Created by Zejun Chen on 2022/4/13.
//


import CoreData
import LeanCloud

extension WaterfallVC{
    func getDraftNotes(){
//        let draftNotes =  try! context.fetch()
        let request = DraftNote.fetchRequest() as NSFetchRequest<DraftNote>
//        request.fetchLimit = 20
//        request.fetchOffset = 0
        
        //筛选
//        request.predicate = NSPredicate(format: "title = %@", " ")
        
        let sortDescriptor1 = NSSortDescriptor(key: "updatedAt", ascending: false)
//        let sortDescriptor2 = NSSortDescriptor(key: "title", ascending: true)
        request.sortDescriptors = [sortDescriptor1]
        request.propertiesToFetch = ["coverPhoto","title", "updatedAt","isVideo"]//放字符串，只放需要取的东西，可以提高性能
        
//        request.returnsObjectsAsFaults
        
        showLoadHUD()
        backgroundContext.perform {
            if let draftNotes = try? backgroundContext.fetch(request){
                self.draftNotes = draftNotes
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }
            }
            self.hideLoadHUD()
        }
    }
    
    @objc func getNotes(){
        let query = LCQuery(className: kNoteTable)
        //查询当前横滑tab是channel值的数据
        query.whereKey(kChannelCol, .equalTo(channel))
        query.whereKey(kAuthorCol, .included)
        query.whereKey(kUpdatedAtCol, .descending)
        query.limit = kNoteOffset
        
        query.find { result in
            if case let .success(objects: notes) = result{
                self.notes = notes
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }
            }
            DispatchQueue.main.async {
                self.header.endRefreshing()
            }
        }
    }
    
    @objc func getMyNotes(){
        let query = LCQuery(className: kNoteTable)
        //查询当前横滑tab是channel值的数据
        query.whereKey(kAuthorCol, .equalTo(user!))
        query.whereKey(kAuthorCol, .included)
        query.whereKey(kUpdatedAtCol, .descending)
        query.limit = kNoteOffset
        
        query.find { result in
            if case let .success(objects: notes) = result{
                self.notes = notes
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }
            }
            DispatchQueue.main.async {
                self.header.endRefreshing()
            }
        }
    }
    
    
    @objc func getMyFavNotes(){
        getFavOrLike(kUserFavTable)
    }
    
    @objc func getMyLikeNotes(){
        getFavOrLike(kUserLikeTable)
    }
    
    
    private func getFavOrLike(_ className: String){
        let query = LCQuery(className: className)
        query.whereKey(kUserCol, .equalTo(user!))
        query.whereKey(kNoteCol, .selected)
        query.whereKey(kNoteCol, .included)
        query.whereKey("\(kNoteCol).\(kAuthorCol)", .included)
        query.whereKey(kUpdatedAtCol, .descending)
        query.limit = kNoteOffset
        query.find { res in
            if case let .success(objects: userFavOrLikes) = res{
                //收藏的所有笔记
                self.notes = userFavOrLikes.compactMap{ $0.get(kNoteCol) as? LCObject }
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }
            }
            DispatchQueue.main.async {
                self.header.endRefreshing()
            }
        }
    }
}


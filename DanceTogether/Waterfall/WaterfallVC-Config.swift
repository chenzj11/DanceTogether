//
//  WaterfallVC-Config.swift
//  DanceTogether
//
//  Created by Zejun Chen on 2022/4/13.
//

import CHTCollectionViewWaterfallLayout
import UIKit

extension WaterfallVC{
    func config(){
        let layout = collectionView.collectionViewLayout as! CHTCollectionViewWaterfallLayout
        
        layout.columnCount = 2
        layout.minimumColumnSpacing = kWaterfallPadding
        layout.minimumInteritemSpacing = kWaterfallPadding
        var inset: UIEdgeInsets = .zero
        if let _ = user{
            layout.sectionInset = UIEdgeInsets(top: 10, left: kWaterfallPadding, bottom: kWaterfallPadding, right: kWaterfallPadding)
        }else{
            layout.sectionInset = UIEdgeInsets(top: 0, left: kWaterfallPadding, bottom: kWaterfallPadding, right: kWaterfallPadding)
        }
        layout.itemRenderDirection = .shortestFirst
        layout.sectionInset = inset
        
//        if isDraft {
//            layout.sectionInset = UIEdgeInsets(top: 44, left: kWaterfallPadding, bottom: kWaterfallPadding, right: kWaterfallPadding)
//        }
        if isDraft{
            navigationItem.title = "本地草稿"
            navigationController?.navigationBar.tintColor = .label
        }
        
        collectionView.register(UINib(nibName: "MyDraftNoteWaterfallCell", bundle: nil), forCellWithReuseIdentifier: kMyDraftNoteWaterfallCellID)
        
        collectionView.mj_header = header
    }
}

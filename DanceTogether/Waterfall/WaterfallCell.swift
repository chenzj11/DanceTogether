//
//  WaterfallCell.swift
//  DanceTogether
//
//  Created by Zejun Chen on 2022/3/21.
//

import UIKit
import LeanCloud
import Kingfisher

class WaterfallCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var nickNameLabel: UILabel!
    @IBOutlet weak var likeBtn: UIButton!
    
    var isMyselfLike = false
    
    var likeCount = 0{
        didSet{
            likeBtn.setTitle(likeCount.formattedStr, for: .normal)
        }
    }
    //记录真实的点击次数
    var currentLikeCount = 0
    
    var isLike: Bool{ likeBtn.isSelected }
    
    var note: LCObject?{
        didSet{
            guard let note = note, let author = note.get(kAuthorCol) as? LCUser else { return }
            
            let coverPhotoURL = note.getImageURL(from: kCoverPhotoCol, .coverPhoto)
            imageView.kf.setImage(with: coverPhotoURL, options: [.transition(.fade(0.2))])
            
            let avatarURL = author.getImageURL(from: kAvatarCol, .avatar)
            avatarImageView.kf.setImage(with:avatarURL)
            
            titleLabel.text = note.getExactStringVal(kTitleCol)
            nickNameLabel.text = author.getExactStringVal(kNickNameCol)
            //笔记被赞数
            likeCount = note.getExactIntVal(kLikeCountCol)
            currentLikeCount = likeCount
            
            //待做：点赞和判断是否点赞
            if isMyselfLike{
                likeBtn.isSelected = true
            }else{
                if let user = LCApplication.default.currentUser{
                    let query = LCQuery(className: kUserLikeTable)
                    query.whereKey(kUserCol, .equalTo(user))
                    query.whereKey(kNoteCol, .equalTo(note))
                    //有且只有一条
                    query.getFirst{ res in
                        if case .success = res{
                            DispatchQueue.main.async {
                                self.likeBtn.isSelected = true
                                
                            }
                        }
                    }
                }
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let icon = UIImage(systemName: "heart.fill")?.withTintColor(mainColor, renderingMode: .alwaysOriginal)
        likeBtn.setImage(icon, for: .selected)
        
//无效：测试结果得awakefromnib先执行，cellforitemat后执行，ismyselflike传不出去，deque的时候就已经调用awakefromnib了
//        if isMyselfLike{
//            likeBtn.isSelected = true
//        }
    }
    
    @IBAction func like(_ sender: Any) {
        if let _ = LCApplication.default.currentUser{
            likeBtn.isSelected.toggle()
            isLike ? (likeCount += 1) : (likeCount -= 1)
            
            
            //数据
            //将网络请求放到objc的函数里面去，避免多次点击（优化）（防止暴力点击）
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(likeBtnTappedWhenLogin), object: nil)
            //三秒内只处理一次
            perform(#selector(likeBtnTappedWhenLogin), with: nil, afterDelay: 1)
            
        }else{
            showGlobalTextHUD("请先登录哦")
        }
    }
    
    
    
}

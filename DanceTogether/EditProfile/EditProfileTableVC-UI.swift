//
//  EditProfileTableVC-UI.swift
//  DanceTogether
//
//  Created by Zejun Chen on 2022/5/10.
//

import Kingfisher

extension EditProfileTableVC{
    func setUI(){
        avatarImageView.kf.setImage(with: user.getImageURL(from: kAvatarCol, .avatar))
        
        nickName = user.getExactStringVal(kNickNameCol)
        
        gender = user.getExactBoolValDefaultF(kGenderCol)
        
        birth = user.get(kBirthCol)?.dateValue
        
        intro = user.getExactStringVal(kIntroCol)
    }
}

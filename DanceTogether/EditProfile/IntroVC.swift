//
//  IntroVC.swift
//  DanceTogether
//
//  Created by Zejun Chen on 2022/5/10.
//

import UIKit

class IntroVC: UIViewController {
    
    //正向传值
    var intro = ""
    var delegate: IntroVCDelegate?

    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var countLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        textView.becomeFirstResponder()
        textView.text = intro
        countLabel.text = "\(kMaxIntroCount)"
    }
    @IBAction func done(_ sender: Any) {
        
        
        delegate?.updateIntro(textView.exactText)
        
        dismiss(animated: true)
    }
}

extension IntroVC: UITextViewDelegate{
    func textViewDidChange(_ textView: UITextView) {
        guard textView.markedTextRange == nil else { return }
        countLabel.text = "\(kMaxIntroCount - textView.text.count)"
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let isExceed = range.location >= kMaxIntroCount || (textView.text.count + text.count) > kMaxIntroCount
        if isExceed { showTextHUD("个人简介最多输入100字哦") }
        return !isExceed
    }
}

//
//  NoteDetailVC-EditNote.swift
//  DanceTogether
//
//  Created by Zejun Chen on 2022/5/3.
//

import LeanCloud
import Kingfisher

extension NoteDetailVC{
    func editNote(){
        //从缓存（内存）中获取当前笔记的图片
        var photos:[UIImage] = []
        
        if let coverPhotoPath = (note.get(kCoverPhotoCol) as? LCFile)?.url?.stringValue,
           let coverPhoto = ImageCache.default.retrieveImageInMemoryCache(forKey: coverPhotoPath){
            photos.append(coverPhoto)
        }
        
        if let photoPaths = note.get(kPhotosCol)?.arrayValue as? [String]{
            let otherPhotos = photoPaths.compactMap{ ImageCache.default.retrieveImageInMemoryCache(forKey: $0) }
            photos.append(contentsOf: otherPhotos)
        }
        
        //视频缓存不做了
        
        let vc = storyboard!.instantiateViewController(identifier: kNoteEditVCID) as! NoteEditVC
        vc.note = note
        vc.photos = photos
        //视频缓存不做了
        vc.videoURL = nil
        vc.updateNoteFinished = { noteID in
            let query = LCQuery(className: kNoteTable)
            query.get(noteID){ res in
                if case let .success(object: note) = res{
                    self.note = note
                    
                    self.showNote(true)
                }
            }
        }
        
        present(vc, animated:  true)
    }
}

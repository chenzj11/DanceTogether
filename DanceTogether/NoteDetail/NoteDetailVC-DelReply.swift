//
//  NoteDetailVC-DelReply.swift
//  DanceTogether
//
//  Created by Zejun Chen on 2022/5/6.
//

import LeanCloud
import Foundation

extension NoteDetailVC{
    func delReply(_ reply: LCObject, _ indexPath: IndexPath){
        showDelAlert(for: "回复"){ _ in
            //云端数据
            reply.delete{ _ in }
            self.updateCommentCount(by: -1)
            
            //内存数据
            self.replies[indexPath.section].replies.remove(at: indexPath.row)
            
            //UI
            self.tableView.reloadData()
        }
        
        
    }
}

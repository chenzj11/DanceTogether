//
//  NoteDetailVC-LikeFav.swift
//  DanceTogether
//
//  Created by Zejun Chen on 2022/4/29.
//
import LeanCloud
import Darwin

extension NoteDetailVC{
    func like(){
        if let _ = LCApplication.default.currentUser{
            //UI
            isLike ? (likeCount += 1) : (likeCount -= 1)
            
            //数据
            //防暴力点击
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(likeBtnTappedWhenLogin), object: nil)
            //三秒内只处理一次
            perform(#selector(likeBtnTappedWhenLogin), with: nil, afterDelay: 1)
            
        }else{
            showLoginHUD()
        }
    }
    
    @objc private func likeBtnTappedWhenLogin(){
        if likeCount != currentLikeCount{
            let user = LCApplication.default.currentUser!
            let authorObjectId = author?.objectId?.stringValue ?? ""
            
            let offset = isLike ? 1 : -1
            currentLikeCount += offset
            if isLike{
                let userLike = LCObject(className: kUserLikeTable)
                try? userLike.set(kUserCol, value: user)
                try? userLike.set(kNoteCol, value: note)
                userLike.save{ _ in }
                
                //原子操作
                try? note.increase(kLikeCountCol)
                
                //不允许修改别人的user所以用userinfo
                LCObject.userInfo(where: authorObjectId, increase: kLikeCountCol)
                
                //个人获得的的点赞和收藏数量，由于LC不能够修改别的用户的数据，所以没办法做
            }else{
                let query = LCQuery(className: kUserLikeTable)
                query.whereKey(kUserCol, .equalTo(user))
                query.whereKey(kNoteCol, .equalTo(note))
                //有且只有一条
                query.getFirst{ res in
                    if case let .success(object: userLike) = res{
                        userLike.delete{ _ in }
                    }
                }
                
                try? note.set(kLikeCountCol, value: likeCount)
                note.save { _ in }
                LCObject.userInfo(where: authorObjectId, decrease: kLikeCountCol, to: likeCount)
            }
        }
    }
}

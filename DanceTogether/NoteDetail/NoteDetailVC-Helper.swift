//
//  NoteDetailVC-Helper.swift
//  DanceTogether
//
//  Created by Zejun Chen on 2022/5/3.
//
import LeanCloud

extension NoteDetailVC{
    func comment(){
        if let _ = LCApplication.default.currentUser{
            showTextView()
        }else{
            showLoginHUD()
        }
    }
    
    func prepareForReply(_ nickName: String, _ section: Int, _ replyToUser: LCUser? = nil){
        
        showTextView(true, "回复\(nickName)", replyToUser)
        commentSection = section
    }
    
    func showTextView(_ isReply: Bool = false ,_ textViewPH: String = kNoteCommentPH, _ replyToUser: LCUser? = nil){
        //reset
        self.isReply = isReply
        textView.placeholder = textViewPH
        self.replyToUser = replyToUser
        
        textView.becomeFirstResponder()
        textViewBarView.isHidden = false
    }
    
    func hideAndResetTextView(){
        textView.resignFirstResponder()
        textView.text = ""
    }
}

extension NoteDetailVC{
    //不管是删除笔记还是删除评论都可以使用
    func showDelAlert(for name: String, confirmHandler: ((UIAlertAction) -> ())?){
        let alert = UIAlertController(title: "提示", message: "确认删除此\(name)", preferredStyle: .alert)
        let action1 = UIAlertAction(title: "取消", style: .cancel)
        let action2 = UIAlertAction(title: "确认", style: .default, handler: confirmHandler)
        alert.addAction(action1)
        alert.addAction(action2)
        present(alert, animated: true)
    }
    
    func updateCommentCount(by offset:Int){
        //云端数据
        try? self.note.increase(kCommentCountCol, by: offset)
        self.note.save{ _ in}
        //UI
        self.commentCount += offset
    }
    
    func backToCell(){
        if let cellItem = cellItem{
            delegate?.updateLikedBtn(cellItem: cellItem, isLike: isLike, likeCount: likeCount)
        }
        dismiss(animated: true)
    }
}

//
//  NoteDetailVC-DelComment.swift
//  DanceTogether
//
//  Created by Zejun Chen on 2022/5/6.
//

import Foundation
import LeanCloud

extension NoteDetailVC{
    func delComment(_ comment: LCObject, _ section: Int){
            showDelAlert(for: "评论"){ _ in
            //云端数据
            comment.delete{ _ in}
            self.updateCommentCount(by: -1)
            //内存数据
            self.comments.remove(at: section)
            //UI
            //避免出现indexoutofrange
            self.tableView.reloadData()

        }
    }
}

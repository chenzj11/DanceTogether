//
//  NoteDetailVC-MeVC.swift
//  DanceTogether
//
//  Created by Zejun Chen on 2022/5/9.
//

import LeanCloud
import Hero

extension NoteDetailVC{
    func noteToMeVC(_ user: LCUser?){
        guard let user = user else { return }
        
        if isFromMeVC, let fromMeVCUser = fromMeVCUser, fromMeVCUser == user{
            dismiss(animated: true)
        }else{
            let meVC = storyboard!.instantiateViewController(identifier: kMeVCID) { coder in
                MeVC(coder: coder, user: user)
            }
            meVC.isFromNote = true
            meVC.modalPresentationStyle = .fullScreen
            meVC.heroModalAnimationType = .selectBy(presenting: .push(direction: .left), dismissing: .pull(direction: .right))
            present(meVC, animated: true)
        }
    }
    
    @objc func goToMeVC(_ tap: UIPassableTapGestureRecognizer){
        let user = tap.passObj
        noteToMeVC(user)
    }
}

//
//  NoteDetailVC-Config.swift
//  DanceTogether
//
//  Created by Zejun Chen on 2022/4/27.
//

import LeanCloud
import ImageSlideshow
import GrowingTextView
import UIKit
import Hero

extension NoteDetailVC{
    func config(){
        
        imageSlideshow.zoomEnabled = true
        imageSlideshow.circular = false
        imageSlideshow.contentScaleMode = .scaleAspectFit
        imageSlideshow.activityIndicator = DefaultActivityIndicator()
        let pageControl = UIPageControl()
        pageControl.pageIndicatorTintColor = .systemGray
        pageControl.currentPageIndicatorTintColor = mainColor
        imageSlideshow.pageIndicator = pageControl
        
        //
        if LCApplication.default.currentUser == nil{
            likeBtn.setToNormal()
            favBtn.setToNormal()
        }
        
        //textView
        textView.textContainerInset = UIEdgeInsets(top: 11.5, left: 16, bottom: 11.5, right: 16)
        textView.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChangeFrame), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        
        tableView.register(UINib(nibName: "CommentView", bundle: nil), forHeaderFooterViewReuseIdentifier: kCommentViewID)
        tableView.register(CommentSectionFooterView.self, forHeaderFooterViewReuseIdentifier: kCommentSectionFooterViewID)
        
        view.hero.id = noteHeroID
        
        let pan = UIPanGestureRecognizer(target: self, action: #selector(slide))
        view.addGestureRecognizer(pan)
    }
    
    func adjustTableHeaderViewHeight(){
        //子视图都渲染完后触发
        let height = tableHeaderView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
        var frame = tableHeaderView.frame
        
        if frame.height != height{
            frame.size.height = height
            tableHeaderView.frame = frame
        }
    }
}

extension NoteDetailVC: GrowingTextViewDelegate{
    func textViewDidChangeHeight(_ textView: GrowingTextView, height: CGFloat) {
        UIView.animate(withDuration: 0.2){
            self.view.layoutIfNeeded()
        }
    }
}


extension NoteDetailVC{
    @objc private func keyboardWillChangeFrame(_ notification: Notification){
        if let endFrame = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue{
            let keyboardH = screenRect.height - endFrame.origin.y
            
            if keyboardH > 0{
                view.insertSubview(overlayView, belowSubview: textViewBarView)
            }else{
                overlayView.removeFromSuperview()
                textViewBarView.isHidden = true
            }
            
            textViewBarBottomConstraint.constant = -keyboardH
            view.layoutIfNeeded()
        }
    }
    
    @objc private func slide(pan: UIPanGestureRecognizer){
        print("不稳定，取消使用")
//        let translationX = pan.translation(in: pan.view).x
//
//        if translationX > 0 {
//
//            let progress = translationX / (screenRect.width / 3)
//
//            switch pan.state{
//            case .began:
//                backToCell()
//            case .changed:
//                Hero.shared.update(progress)
//                let position = CGPoint(x: translationX + view.center.x, y: pan.translation(in: pan.view).y + view.center.y)
//                Hero.shared.apply(modifiers: [.position(position)], to: view)
//            default:
//                //当用户快速右滑的时候也finish整个交互动画
//                if progress + pan.velocity(in: pan.view).x / view.bounds.width > 0.5 {
//                    Hero.shared.finish()
//                }else{
//                    Hero.shared.cancel()
//                }
//            }
//
//        }else if translationX < 0 {
//            let progress = -(translationX / screenRect.width)
//            switch pan.state {
//            case .began:
//                noteToMeVC(author)
//            case .changed:
//                Hero.shared.update(progress)
//            default:
//                if progress > 0.2{
//                    Hero.shared.finish()
//                }else{
//                    Hero.shared.cancel()
//                }
//            }
//        }
    }
    

}

//
//  NoteDetailVC-TVDelegate.swift
//  DanceTogether
//
//  Created by Zejun Chen on 2022/5/5.
//

import LeanCloud
import UIKit

extension NoteDetailVC: UITableViewDelegate{
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let commentView = tableView.dequeueReusableHeaderFooterView(withIdentifier: kCommentViewID) as! CommentView
        
        let comment = comments[section]
        let commentAuthor = comment.get(kUserCol) as? LCUser
        
        commentView.comment =  comment
        
        if let commentAuthor = commentAuthor, let noteAuthor = author, commentAuthor == noteAuthor{
            commentView.authorLabel.isHidden = false
        }
        
        let commentTap = UITapGestureRecognizer(target: self, action: #selector(commentTapped))
        commentView.tag = section
        commentView.addGestureRecognizer(commentTap)
        
        //轻触手势已经被扩展
        let avatarTap = UIPassableTapGestureRecognizer(target: self, action:#selector(goToMeVC))
        avatarTap.passObj = commentAuthor
        commentView.avatarImageView.addGestureRecognizer(avatarTap)
        
        let nickNameTap =  UIPassableTapGestureRecognizer(target: self, action:#selector(goToMeVC))
        avatarTap.passObj = commentAuthor
        commentView.nickNameLabel.addGestureRecognizer(nickNameTap)
        
        return commentView
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let separatorLine = tableView.dequeueReusableHeaderFooterView(withIdentifier: kCommentSectionFooterViewID)
        return separatorLine
    }
    
    //用户按下评论的回复cell后，对这个回复进行复制或删除或再回复
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let user = LCApplication.default.currentUser{
            let reply = replies[indexPath.section].replies[indexPath.row]
            
            guard let replyAuthor = reply.get(kUserCol) as? LCUser else { return }
            let replyAuthorNickName = replyAuthor.getExactStringVal(kNickNameCol)

            
            if replyAuthor == user{
                let replyText = reply.getExactStringVal(kTextCol)
                
                let alert = UIAlertController(title: nil, message: "你的回复 \(replyText)", preferredStyle: .actionSheet)
                let subReplyAction = UIAlertAction(title: "回复", style: .default){ _ in
                    //回复
                    self.prepareForReply(replyAuthorNickName, indexPath.section, replyAuthor)
                }
                let copyAction = UIAlertAction(title: "复制", style: .default){ _ in
                    UIPasteboard.general.string = replyText
                }
                let deleteAction = UIAlertAction(title: "删除", style: .destructive){ _ in
                    self.delReply(reply, indexPath)
                }
                let cancelAction = UIAlertAction(title: "取消", style: .cancel, handler: nil)
                
                alert.addAction(subReplyAction)
                alert.addAction(copyAction)
                alert.addAction(deleteAction)
                alert.addAction(cancelAction)
                
                present(alert, animated: true, completion: nil)
            }else{
                //当前用户点击别人的回复
                self.prepareForReply(replyAuthorNickName, indexPath.section, replyAuthor)
            }
            
            
        }else{
            showLoginHUD()
        }
    }
}


extension NoteDetailVC{
    @objc private func commentTapped(_ tap: UITapGestureRecognizer){
        if let user = LCApplication.default.currentUser{
            guard let section = tap.view?.tag else { return }
            let comment = comments[section]
            guard let commentAuthor = comment.get(kUserCol) as? LCUser else { return }
            let commentAuthorNickName = commentAuthor.getExactStringVal(kNickNameCol)
            
            if commentAuthor == user{
                
                let commentText = comment.getExactStringVal(kTextCol)
                
                let alert = UIAlertController(title: nil, message: "你的评论 \(comment.getExactStringVal(kTextCol))", preferredStyle: .actionSheet)
                let replyAction = UIAlertAction(title: "回复", style: .default){ _ in
                    //回复
                    self.prepareForReply(commentAuthorNickName, section)
                }
                let copyAction = UIAlertAction(title: "复制", style: .default){ _ in
                    UIPasteboard.general.string = commentText
                }
                let deleteAction = UIAlertAction(title: "删除", style: .destructive){ _ in
                    self.delComment(comment, section)
                    
                }
                let cancelAction = UIAlertAction(title: "取消", style: .cancel, handler: nil)
                
                alert.addAction(replyAction)
                alert.addAction(copyAction)
                alert.addAction(deleteAction)
                alert.addAction(cancelAction)
                
                present(alert, animated: true, completion: nil)
                
            }else{
                //回复
                self.prepareForReply(commentAuthorNickName, section)
            }
            
        }else{
            showLoginHUD()
        }
    }
}

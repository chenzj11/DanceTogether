//
//  CommentView.swift
//  DanceTogether
//
//  Created by Zejun Chen on 2022/5/5.
//

import UIKit
import LeanCloud

class CommentView: UITableViewHeaderFooterView {

    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nickNameLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var commentTextLabel: UILabel!
    
    var comment: LCObject?{
        didSet{
            guard let comment = comment else{ return }
            
            if let user = comment.get(kUserCol) as? LCUser{
                avatarImageView.kf.setImage(with: user.getImageURL(from: kAvatarCol, .avatar))
                nickNameLabel.text = user.getExactStringVal(kNickNameCol)
            }
            
            let commentText = comment.getExactStringVal(kTextCol)
            let createdAt = comment.createdAt?.value
            let dateText = comment.createdAt?.value == nil ? "刚刚" : createdAt!.formattedDate
            
            commentTextLabel.attributedText =  commentText.spliceAttrStr(dateText)
        }
    }

}

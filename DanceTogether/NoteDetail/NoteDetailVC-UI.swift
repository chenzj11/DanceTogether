//
//  NoteDetailVC-UI.swift
//  DanceTogether
//
//  Created by Zejun Chen on 2022/4/27.
//

import Kingfisher
import LeanCloud
import ImageSlideshow
import SwiftUI


extension NoteDetailVC{
    func setUI(){
        followBtn.makeCapsule(mainColor)
        
        if isReadMyNote{
            followBtn.isHidden = true
            shareOrMoreBtn.setImage(UIImage(systemName: "ellipsis"), for: .normal)
        }
        
        showNote()
        showLike()
    }
    
    func showNote(_ isUpdatingNote: Bool = false){
        //上方bar的author
        if !isUpdatingNote{
            let authorAvatarURL = author?.getImageURL(from: kAvatarCol, .avatar)
            authorAvatarBtn.kf.setImage(with: authorAvatarURL, for: .normal)
            authorNickNameBtn.setTitle(author?.getExactStringVal(kNickNameCol), for: .normal)
        }
        
        //note图片
        let coverPhotoHeight = CGFloat(note.getExactDoubleVal(kCoverPhotoRatioCol)) * screenRect.width
        imageSlideShowHeight.constant = coverPhotoHeight
        
        
        let coverPhoto = KingfisherSource(url: note.getImageURL(from: kCoverPhotoCol, .coverPhoto))
        if let photoPaths = note.get(kPhotosCol)?.arrayValue as? [String]{
            var photoArr = photoPaths.compactMap{ KingfisherSource(urlString: $0) }
            photoArr[0] = coverPhoto
            imageSlideshow.setImageInputs(photoArr)
        }else{
            imageSlideshow.setImageInputs([coverPhoto])
        }
        
        //note标题
        let noteTitle = note.getExactStringVal(kTitleCol)
        if noteTitle.isEmpty{
            titleLabel.isHidden = true
        }else{
            titleLabel.text = noteTitle
        }
        
        //note正文
        let noteText = note.getExactStringVal(kTextCol)
        if noteText.isEmpty{
            textLabel.isHidden = true
        }else{
            textLabel.text = noteText
        }
        
        let noteChannel = note.getExactStringVal(kChannelCol)
        let noteSubChannel = note.getExactStringVal(kSubChannelCol)
        channelBtn.setTitle(noteSubChannel.isEmpty ? noteChannel : noteSubChannel, for: .normal)
        
        //要判断是否被编辑过
        if let updatedAt = note.updatedAt?.value{
            dateLabel.text = "\(note.getExactBoolValDefaultF(kHasEditCol) ? "编辑于 " : "")\( updatedAt.formattedDate)"
        }
        
        //当前用户头像
        if let user = LCApplication.default.currentUser{
            let avatarURL = user.getImageURL(from: kAvatarCol, .avatar)
            avatarImageView.kf.setImage(with: avatarURL)
        }
        
        //点赞收藏评论
        //点赞数
        likeCount = note.getExactIntVal(kLikeCountCol)
        currentLikeCount = likeCount
        //收藏数
        favCount = note.getExactIntVal(kFavCountCol)
        currentFavCount = favCount
        //评论数
        commentCount = note.getExactIntVal(kCommentCountCol)
    }
    
    private func showLike(){
        if isFromPush{
            if let user = LCApplication.default.currentUser{
                let query = LCQuery(className: kUserLikeTable)
                query.whereKey(kUserCol, .equalTo(user))
                query.whereKey(kNoteCol, .equalTo(note))
                //有且只有一条
                query.getFirst{ res in
                    if case .success = res{
                        DispatchQueue.main.async {
                            self.likeBtn.setSelected(selected: true, animated: false)
                            
                        }
                    }
                }
            }
        }else{
            self.likeBtn.setSelected(selected: self.isLikeFromWaterfallCell, animated: false)
        }
    }
}

//
//  NoteDetailVC-Fav.swift
//  DanceTogether
//
//  Created by Zejun Chen on 2022/5/3.
//

import LeanCloud

extension NoteDetailVC{
    func fav(){
        if let _ = LCApplication.default.currentUser{
            //UI
            isFav ? (favCount += 1) : (favCount -= 1)
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(favBtnTappedWhenLogin), object: nil)
            //三秒内只处理一次
            perform(#selector(favBtnTappedWhenLogin), with: nil, afterDelay: 1)
        }else{
            showLoginHUD()
        }
    }
    
    @objc private func favBtnTappedWhenLogin(){
        if favCount != currentFavCount{
            let user = LCApplication.default.currentUser!
            let authorObjectId = author?.objectId?.stringValue ?? ""
            
            let offset = isFav ? 1 : -1
            currentFavCount += offset
            if isFav{
                let userFav = LCObject(className: kUserFavTable)
                try? userFav.set(kUserCol, value: user)
                try? userFav.set(kNoteCol, value: note)
                userFav.save{ _ in }
                
                //原子操作
                try? note.increase(kFavCountCol)
                LCObject.userInfo(where: authorObjectId, increase: kFavCountCol)
                
                //个人获得的的点赞和收藏数量，由于LC不能够修改别的用户的数据，所以没办法做
            }else{
                let query = LCQuery(className: kUserFavTable)
                query.whereKey(kUserCol, .equalTo(user))
                query.whereKey(kNoteCol, .equalTo(note))
                //有且只有一条
                query.getFirst{ res in
                    if case let .success(object: userFav) = res{
                        userFav.delete{ _ in }
                    }
                }
                
                try? note.set(kFavCountCol, value: favCount)
                note.save { _ in }
                LCObject.userInfo(where: authorObjectId, decrease: kFavCountCol, to: favCount)
            }
        }
    }
}



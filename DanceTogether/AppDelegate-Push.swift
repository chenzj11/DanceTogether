//
//  AppDelegate-Push.swift
//  DanceTogether
//
//  Created by Zejun Chen on 2022/5/11.
//

import LeanCloud

extension AppDelegate{
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let installation = LCApplication.default.currentInstallation
        
        installation.set(
            deviceToken: deviceToken,
            apnsTeamId: "9554ZKPJS6")
        if installation.get(kUserCol) == nil, let user = LCApplication.default.currentUser{
            try? installation.set(kUserCol, value: user)
        }
        
        installation.save{ _ in }
        }
    }


extension AppDelegate : UNUserNotificationCenterDelegate{
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        let userInfo = response.notification.request.content.userInfo
        if let noteID = userInfo["noteID"] as? String{
            let query = LCQuery(className: kNoteTable)
            query.whereKey(kAuthorCol, .included)
            UIViewController.showGlobalLoadHUD()
            query.get(noteID){ res in
                UIViewController.hideGlobalHUD()
                if case let .success(object: note) = res{
                    
                    guard let tabbarC = (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.window?.rootViewController as? UITabBarController else { return }
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let detailVC = storyboard.instantiateViewController(identifier: kNoteDetailVCID){ coder in
                       NoteDetailVC(coder: coder, note: note)
                    }
                    detailVC.modalPresentationStyle = .fullScreen
                    detailVC.isFromPush = true
                    tabbarC.selectedViewController?.present(detailVC, animated: true)
                }
            }
        }
        
        completionHandler()
    }
}

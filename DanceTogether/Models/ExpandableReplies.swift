//
//  ExpandableReplies.swift
//  DanceTogether
//
//  Created by Zejun Chen on 2022/5/6.
//

import LeanCloud

struct ExpandableReplies{
    var isExpanded = false
    var replies: [LCObject]
}

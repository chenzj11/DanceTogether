//
//  CustomViews.swift
//  DanceTogether
//
//  Created by Zejun Chen on 2022/4/27.
//

import Foundation
import UIKit

@IBDesignable
class BigButton: UIButton{
    @IBInspectable var cornerRadius: CGFloat = 0{
        didSet{
            layer.cornerRadius = cornerRadius
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        sharedInit()
    }
    
    required init?(coder: NSCoder){
        super.init(coder: coder)
        sharedInit()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        sharedInit()
    }
    
    private func sharedInit(){
        backgroundColor = .secondarySystemBackground
        tintColor = .placeholderText
        setTitleColor(.placeholderText, for: .normal)
        
        contentHorizontalAlignment = .leading
        contentEdgeInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 0)
    }
}


@IBDesignable
class RoundedLabel: UILabel{
    override func draw(_ rect: CGRect){
        super.drawText(in: rect.inset(by: UIEdgeInsets(top: 0, left: 35, bottom: 0, right: 5)))
    }
}

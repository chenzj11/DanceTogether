//
//  Constants.swift
//  DanceTogether
//
//  Created by Zejun Chen on 2022/3/17.
//

import Foundation
import UIKit

// MARK: StoryboardID
let kFollowVCID = "FollowVCID"
let kNearbyVCID = "NearbyVCID"
let kDiscoveryVCID = "DiscoveryVCID"
let kWaterfallVCID = "WaterfallVCID"
let kNoteEditVCID = "NoteEditVCID"
let kChannelTableVCID = "ChannelTableVCID"
let kLoginNaviID = "LoginNaviID"
let kLoginVCID = "LoginVCID"
let kMeVCID = "MeVCID"
let kDraftNotesNaviID = "DraftNotesNaviID"
let kNoteDetailVCID = "NoteDetailVCID"
let kIntroVCID = "IntroVCID"
let kEditProfileNaviID = "EditProfileNaviID"
let kSettingTableVCID = "SettingTableVCID"

// MARK: Cell相关ID
let kWaterfallCellID = "WaterfallCellID"
let kPhotoCellID = "PhotoCellID"
let kPhotoFooterID = "PhotoFooterID"
let kSubChannelCellID = "SubChannelCellID"
let kPOICellID = "POICellID"
let kDraftNoteWaterfallCellID = "DraftNoteWaterfallCellID"
let kMyDraftNoteWaterfallCellID = "MyDraftNoteWaterfallCellID"
let kCommentViewID = "CommentViewID"
let kReplyCellID = "ReplyCellID"
let kCommentSectionFooterViewID = "CommentSectionFooterViewID"

//MARK: - 资源文件相关
let mainColor = UIColor(named: "main")!
let blueColor = UIColor(named: "blue")!
let mainLightColor = UIColor(named: "main-light")
let imagePH = UIImage(named: "launchScreen-light")!

//MARK: - UserDefault
let kNameFromAppleID = "nameFromAppleID"
let kEmailFromAppleID = "emailFromAppleID"
let kDraftNoteCount = "draftNoteCount"
let kUserInterfaceStyle = "userInterfaceStyle"

//MARK: - CoreData
let appDelegate = UIApplication.shared.delegate as! AppDelegate
let persistentContainer = appDelegate.persistentContainer
let context = appDelegate.persistentContainer.viewContext
let backgroundContext = persistentContainer.newBackgroundContext()

//MARK: -UI布局相关
let screenRect = UIScreen.main.bounds

//MARK:  -  业务逻辑相关
let kChannels = ["Jazz","Hiphop","Poppin","Kpop","Breaking"]


//瀑布流
let kWaterfallPadding: CGFloat = 4
let kDraftNoteWaterfallCellBottomViewH: CGFloat = 64
let kWaterfallCellBottomView:CGFloat = 64

//YPImagePicker
let kMaxCameraZoomFactor:CGFloat = 5
let kMaxPhotoCount = 9
let kSpacingBetweenItems: CGFloat = 2
let kMinWidthForItem:CGFloat = 1
let kNumberOfItemsInRow = 4


//笔记
let kMaxNoteTitleCount = 20
let kMaxNoteTextCount = 1000
let kNoteCommentPH = "精彩评论将被优先展示哦"

//用户
let kMaxIntroCount = 100
let kIntroPH = "填写个人简介更容易获得关注哦，点击此处填写"
let kNoCachePH = "无缓存"

//话题
let kAllSubChannels = [
    ["JAZZ秘籍", "怎么做wave", "爵士解析"],
    ["一起来嘻哈", "step全集", "嘻哈王者"],
    ["脱离抽搐", "核心发力"],
    ["2022女团", "黑曼巴挑战"],
    ["托马斯", "帅气bboy"]
]

//高德
let kNoPOIPH = "未知地点"
let kPOITypes = "医疗保健服务"
//let kPOITypes = "汽车服务|汽车销售|汽车维修|摩托车服务|餐饮服务|购物服务|生活服务|体育休闲服务|医疗保健服务|住宿服务|风景名胜|商务住宅|政府机构及社会团体|科教文化服务|交通设施服务|金融保险服务|公司企业|道路附属设施|地名地址信息|公共设施"
let kPOIsInitArr = [["不显示位置",""]]
let kPOIsOffset = 20


//极光
let kJAppKey = "2ed2e0fec8402f8409ffccda"

//支付宝
let kAlipayAppID = "2021003127673313"
let kAlipayPID = "2088312887063426"
let kAlipayPrivateKey = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCNqq6ciw1/LlncHc5XeExsqzj5dhZSdp+QP10herYwSSVZwiHaMdn6ukBgLkjBbSQaPAqMZk+cPEDQiluB8MlAwrKVKIzaxQydq2Ua2epQcv5273AVyyKVtE5gGLv7KaO7FYHdx/xMNlJiJ89jwuMe1GcFW0gkBE7n2h+A+rozs4cwM29cad5oDOjygyKD4H1ni9RlubbdUM2gxnviSf42yYq+UpS7eaXhlmDRzUIPgPuz9B93xLxSeYCgvQyJeSoIPbCa3erMb+7R6Un8P6ttSRu63FjaPXyWtsGyghA/AKxSnyRN2hh0qfOa9xOjgs43Ucnm85hrbHOIm6CfivnTAgMBAAECggEAQv7R3sQ/2MKByn/qAiGo+scqhYbb3ZmtshjVIS0YFdOxrzYIrlhe/pywLwwUai0EIsf8MRN9WKSB/l1AEz5c3px0uedYXWlMow/YG4+sqOkJABQRzKypKWUAn59y3r99Z88vAERAQ4Z5BWRQRpx2rjDCm5zki+vhqPHVbjikcCgkPpM/em7Oe4FhIOG3EBMsmLB/K2dCHr/6Dhlo/QD77UP2KdHkSmuxt5AD93cG4xR/hZvJaEe7b76eNBfBWBtjTEVVS2M/fsQTjTPF3Zirc1JIwoFGEHcz+HlyE8BqwYPvD253WBxpxws1PZy3U6qSll9xaFG9GzEWXNqMC84xEQKBgQDWsTEADiIo98Nq40YZwxrWD4vLJridb4cNI+KEJne7/hhlA0zPRmVQ7VCZi8g6OfG44YwPS+pDaremEpWR5KLS6jdR5KlcUdwVOhhpQsk20MduhAY2XgNd1tOuDk1Ba9lYkV9fODULziM6JQXRn7Sq/00wUuN6h14jlIAjKnpQKQKBgQCo7JLLCw72ttpRGTv1+ty59FoBIrw7Yqck+CIQrNzhq0hsqQ6Go1OvMzcnakXE3r3ROOKIc6wesdYCJcP8D8XEfWWm328z+YLsOaIvEzoqeGV1xFygXmXxlkDnrBUJORvGaG8Z4m1JnP6S3RFn8Pkxyi94hA3TQ1DqcqAe3rMJmwKBgF8b5jI5EKcHg9nRT1fQWA4mmu5ne/e61aBbmlii1f5BRbtSJ+/XJsEgMMmk5BHvY4dc2/hW0DQHjc4Tg5E7MWqwectcui580TvSHchb3tnayc7KzBm/skwbBHGiQlVOGt4ioJaFRvKw4mg6yIYh8x6htSdIQSAh4fFvMwjhI4hpAoGBAJyq/OIPmYs7v3Mjwja6uWhFtgwDmSxCrN5eu8lXaWxrHTqZ+HzqkbyV9Xs61wZlxwp6ha8kCOhW6zMGr6PhRPydiF+iLlK4ALLyKPccEAA/tWnp+jZKFqZfQkcblE7/hkEXrqMFG3MAhiLToUcTeSqyaCCP6PqdHjiyosq+pLItAoGBALnExsCSCToT0ouXcCGZuptTzaAPco1r0IlUUhgz8xv6SxtNgBKvawXFc45CY5ETeiZD8ejIrE3nAt4F+ZL2rRSTW9G1LqCyopaY1O+6gl/+i6bOkdDb0AMd2HOUSLSFx0ZT094W+x4niMwD2S0OlG9o7sK08jXFcMKVUCRQvOjx"




let kAppScheme = "DanceTogether"

//正则表达式
let kPhoneRegEx = "^1\\d{10}$"
let kAuthCodeRegEx = "^\\d{6}$"
let kPasswordRegEx = "^[0-9a-zA-Z]{6,16}$"


//云端
let kNoteOffset = 10
let kCommentsOffset = 10

//MARK: - LeanCloud
//LeanCloud
let kLCAppID = "DvfzbV7b7CX5uh4VvVVGbOvO-gzGzoHsz"
let kLCAppKey = "YTltxbF3o8TWT0Iripaft8sV"
let kLCServerURL = "https://dvfzbv7b.lc-cn-n1-shared.com"

//
let kCreatedAtCol = "createdAt"
let kUpdatedAtCol = "updatedAt"


//表
let kNoteTable = "Note"
let kUserLikeTable = "UserLike"
let kUserFavTable = "UserFav"
let kCommentTable = "Comment"
let kReplyTable = "Reply"
let kUserInfoTable = "UserInfo"

//User表
let kNickNameCol = "nickName"
let kAvatarCol = "avatar"
let kGenderCol = "gender"
let kIntroCol = "intro"
let kIDCol = "id"
let kBirthCol = "birth"
let kIsSetPasswordCol = "isSetPassword"
let kNoteCountCol = "noteCount"
 
//Note表
let kCoverPhotoCol = "coverPhoto"
let kPhotosCol = "photos"
let kVideoCol = "video"
let kTitleCol = "title"
let kTextCol = "text"
let kChannelCol = "channel"
let kSubChannelCol = "subChannel"
let kPOINameCol = "poiName"
let kIsVideoCol = "isVideo"
let kLikeCountCol = "likeCount"
let kFavCountCol = "favCount"
let kCommentCountCol = "commentCount"
let kCoverPhotoRatioCol = "coverPhotoRatio"
let kAuthorCol = "author"
let kHasEditCol = "hasEdit"

//UserLike表
let kUserCol = "user"
let kNoteCol = "note"

//Comment表
let kHasReplyCol = "hasReply"

//Reply表
let kCommentCol = "comment"
let kReplyToUserCol = "kReplyToUserCol"

//UserInfo表
let kUserObjectIdCol = "userObjectId"


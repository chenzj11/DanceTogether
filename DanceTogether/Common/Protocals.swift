//
//  Protocal.swift
//  DanceTogether
//
//  Created by Zejun Chen on 2022/3/28.
//

import Foundation
import UIKit

protocol ChannelVCDelegate{
    func updateChannel(channel: String, subChannel:String)
}


protocol POIVCDelegate {
    func updatePOIName(_ poiName: String)
}

protocol IntroVCDelegate{
    func updateIntro(_ intro: String)
}

protocol EditProfileTableVCDelegate {
    func updateUser(_ avatar: UIImage?, _ nickName: String, _ gender: Bool, _ birth: Date?, _ intro:String)
}

protocol NoteDetailVCDelegate{
    func updateLikedBtn(cellItem: Int,isLike: Bool, likeCount: Int)
}

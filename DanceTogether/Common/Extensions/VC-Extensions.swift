//
//  VC-Extensions.swift
//  DanceTogether
//
//  Created by Zejun Chen on 2022/5/11.
//

import Foundation
import UIKit
import DateToolsSwift
import AVFoundation


extension UIViewController{
    //MARK: - 展示加载框或提示框
    //MARK: 加载框--手动隐藏
    func showLoadHUD(_title: String? = nil){
        let hud = MBProgressHUD.showAdded(to: view, animated: true)
        hud.label.text = title
    }
    
    func hideLoadHUD(){
        DispatchQueue.main.async {
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
    
    static func showGlobalLoadHUD(_ title: String? = nil){
        let hud = MBProgressHUD.showAdded(to: UIApplication.shared.windows.last!, animated: true)
        hud.label.text = title
    }
    
    static func hideGlobalHUD(_ title: String? = nil){
        DispatchQueue.main.async {
            MBProgressHUD.hide(for: UIApplication.shared.windows.last!, animated: true)
        }
    }
    
    
    func showTextHUD(_ title:String,_ inCurrentView:Bool = true, _ subTitle:String? = nil){
        var viewToShow = view!
        if !inCurrentView{
            viewToShow = UIApplication.shared.windows.last!
        }
        let hud = MBProgressHUD.showAdded(to: viewToShow, animated: true)
        hud.mode = .text  //不指定的话就会显示菊花和下面的文本
        hud.label.text = title
        hud.detailsLabel.text = subTitle
        hud.hide(animated: true, afterDelay: 2)
    }
    func showLoginHUD(){
        showTextHUD("请先登录哦")
    }
    //在本vc想在别的vc显示小菊花
    func showTextHUD(_ title:String, in view: UIView, _ subTitle:String? = nil){
        let hud = MBProgressHUD.showAdded(to: view, animated: true)
        hud.mode = .text  //不指定的话就会显示菊花和下面的文本
        hud.label.text = title
        hud.detailsLabel.text = subTitle
        hud.hide(animated: true, afterDelay: 2)
    }
    
    func hideKeyBoardWhenTappedAround(){
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false //不加的话会影响用户体验，用户点击其他东西可能变得无效
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard(){
        view.endEditing(true)
    }
    
    func add(child vc: UIViewController){
        addChild(vc)
        //子视图的大小应该完全覆盖父试图控制器
        vc.view.frame = view.bounds
        view.addSubview(vc.view)
        vc.didMove(toParent: self)
    }
    func remove(child vc: UIViewController){
        vc.willMove(toParent: nil)
        vc.view.removeFromSuperview()
        vc.removeFromParent()
    }
    func removeChildren(){
        if !children.isEmpty{
            for vc in children{
                remove(child: vc)
            }
        }
    }
}

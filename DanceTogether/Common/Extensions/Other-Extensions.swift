//
//  Other-Extensions.swift
//  DanceTogether
//
//  Created by Zejun Chen on 2022/5/8.
//

import FaveButton

extension FaveButton{
    func setToNormal(){
        selectedColor = normalColor
        dotFirstColor = normalColor
        dotSecondColor = normalColor
        circleToColor = normalColor
        circleToColor = normalColor
    }
}

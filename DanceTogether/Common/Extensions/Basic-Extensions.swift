//
//  Basic-Extensions.swift
//  DanceTogether
//
//  Created by Zejun Chen on 2022/5/11.
//

import Foundation
import UIKit
import DateToolsSwift
import AVFoundation

extension Int{
    var formattedStr: String{
        let num = Double(self)
        let tenThousand = num / 10000
        let hundredMillion = num / 1_0000_0000
        
        if tenThousand < 1{
            return "\(self)"
        }else if hundredMillion >= 1{
            return "\(round(hundredMillion * 10) / 10)亿"
        }else{
            return "\(round(tenThousand * 10) / 10)万"
        }
    }
}

extension String{
    var isBlank: Bool{
        self.trimmingCharacters(in:  .whitespacesAndNewlines).isEmpty
    }
    
    var isPhoneNum: Bool{
        //判断某个字符串是不是国内的手机号码
        Int(self) != nil && NSRegularExpression(kPhoneRegEx).matches(self)
    }
    
    var isAuthCode: Bool{
        Int(self) != nil && NSRegularExpression(kAuthCodeRegEx).matches(self)
    }
    
    var isPassword: Bool { NSRegularExpression(kPasswordRegEx).matches(self) }
    
    static func randomString(_ length: Int) -> String{
        let letters = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM123456"
        return String((0..<length).map{ _ in letters.randomElement()! })
    }
    
    //用于评论
    func spliceAttrStr(_ dateStr: String) -> NSMutableAttributedString{
        
        
        
        let attrText = toAttrStr()
        
        let attrDate = " \(dateStr)".toAttrStr(12, .secondaryLabel)
        
        attrText.append(attrDate)
        
        return attrText
    }
    
    func toAttrStr(_ fontSize: CGFloat = 14, _ color: UIColor = .label) -> NSMutableAttributedString{
        let attr: [NSAttributedString.Key: Any] = [
            .font: UIFont.systemFont(ofSize: fontSize),
            .foregroundColor: color
        ]
        return NSMutableAttributedString(string: self, attributes: attr)
    }
}

extension NSRegularExpression{
    convenience init(_ pattern: String){
        do {
            try self.init(pattern: pattern)
        } catch {
            fatalError("非法的正则表达式")
        }
    }
    func matches(_ string: String) -> Bool {
        let range = NSRange(location: 0, length: string.utf16.count)
        return firstMatch(in: string,options: [], range: range) != nil
    }
}

extension Date{
    var formattedDate: String{
        let currentYear = Date().year
        
        if year == currentYear{//今年
            
            if isToday{
                
                if minutesAgo > 10{
                    return "今天 \(format(with: "HH:mm"))"
                }else{
                    return timeAgoSinceNow
                }
                
            }else if isYesterday{
                return "昨天 \(format(with: "HH:mm"))"
            }else{
                return format(with: "MM-dd")
            }
            
        }else if year < currentYear{//去年或更早
            return format(with: "yyyy-MM-dd")
        }else{
            return "占位"
        }
    }
}

extension Bundle{
    var appName: String {infoDictionary!["CFBundleDisplayName"] as! String}
    
    static func loadView<T>(fromNib name: String, with type: T.Type) -> T{
        if let view = Bundle.main.loadNibNamed(name, owner: nil,options: nil)?.first as? T{
            return view
        }
        fatalError("加载\(type)类型的view失败")
    }
}

extension Optional where Wrapped == String{
    var unwrappedText: String {self ?? ""}
}

extension URL{
    var thumbnail:UIImage{
        let asset = AVAsset(url: self)
        let assetImgGenerate = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        
        let time = CMTimeMakeWithSeconds(1.0, preferredTimescale: 600)
        do {
            let img = try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
            let thumbnail = UIImage(cgImage: img)
            return thumbnail
        } catch {
            return imagePH
        }
    }
}

extension FileManager{
    func save(_ data: Data?, to dirName: String, as fileName: String) -> URL?{
        guard let data = data else{
            print("要写入的文件为nil")
            return nil
            
        }
        
        let dirURL = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(dirName, isDirectory: true)
        
        if !fileExists(atPath: dirURL.path){
            guard let _ = try? createDirectory(at: dirURL, withIntermediateDirectories: true) else{
                print("创建文件夹失败")
                return nil
            }
        }
        
        let fileURL = dirURL.appendingPathComponent(fileName)
        
        if !fileExists(atPath: fileURL.path){
            guard let _ =  try? data.write(to: fileURL) else{
                print("保存文件失败")
                return nil}
        }
        
        return fileURL
    }
}

extension UserDefaults{
    static func increase(_ key: String, by val: Int = 1){
        standard.set(standard.integer(forKey: key) + val, forKey: key)
    }
    
    static func decrease(_ key: String, by val: Int = 1){
        standard.set(standard.integer(forKey: key) - val, forKey: key)
    }
}

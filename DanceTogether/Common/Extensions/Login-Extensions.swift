//
//  Login-Extensions.swift
//  DanceTogether
//
//  Created by Zejun Chen on 2022/4/26.
//

import Foundation
import UIKit
import LeanCloud

extension UIViewController{
    func configAfterLogin(_ user: LCUser, _ nickName: String, _ email: String = ""){
        
        if let _ = user.get(kNickNameCol){
            dismissAndShowMeVC(user)
        }else{//首次登陆（注册）
            let group = DispatchGroup()
            let randomAvatar = UIImage(named: "avatarPH\(Int.random(in: 1...4))")!
            if let avatarData = randomAvatar.pngData(){
                let avatarFile = LCFile(payload: .data(data: avatarData))
                avatarFile.mimeType = "image/jpeg"
                
                //extension中对file的保存进行了封装
                avatarFile.save(to: user, as: kAvatarCol,group: group)

            }
            
            
            
            do {
                if email != ""{
                    user.email = LCString(email)
                }
                try user.set(kNickNameCol, value: nickName)
            } catch {
                print("给User字段赋值失败：\(error)")
            }
            group.enter()
            user.save { (result) in
                group.leave()
            }
            
            group.enter()
            let userInfo = LCObject(className:  kUserInfoTable)
            try? userInfo.set(kUserObjectIdCol, value: user.objectId)
            userInfo.save{ _ in group.leave() }
            
            group.notify(queue: .main){
                self.dismissAndShowMeVC(user)
            }
        }

    }
    
    func dismissAndShowMeVC(_ user: LCUser){
        hideLoadHUD()
        
        let installation = LCApplication.default.currentInstallation
        try? installation.set(kUserCol, value: user)
        installation.save{ _ in }
        
        DispatchQueue.main.async {
            let mainSB = UIStoryboard(name: "Main", bundle: nil)
            let meVC = mainSB.instantiateViewController(identifier: kMeVCID) { coder in
                MeVC(coder: coder, user: user)
            }
            loginAndMeParentVC.removeChildren()
            loginAndMeParentVC.add(child: meVC)
            self.dismiss(animated: true)
        }
    }
}

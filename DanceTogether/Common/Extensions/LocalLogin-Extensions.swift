//
//  LoginVC-LocalLogin.swift
//  DanceTogether
//
//  Created by Zejun Chen on 2022/4/19.
//

import Foundation
import UIKit
import Alamofire

extension UIViewController{
    
    @objc func localLogin(){
        
        
        showLoadHUD()
        let config = JVAuthConfig()
        config.appKey = kJAppKey
        config.authBlock = { _ in
            if JVERIFICATIONService.isSetupClient(){
                //预取号判断手机支不支持
                JVERIFICATIONService.preLogin(5000) { (result) in
                    self.hideLoadHUD()
                    if let result = result,let code = result["code"] as? Int, code == 7000 {
                        //预取号成功
                        self.presentLocalLoginVC()
                        self.setLocalLoginUI()
                    }else{
                        print("预取号失败，错误码：\(result!["code"])，错误描述：\(result!["code"])")
                        self.presentCodeLoginVC()
                    }
                }
            }else{
                self.hideLoadHUD()
                print("初始化一键登录失败")
                self.presentCodeLoginVC()
            }
        }
        JVERIFICATIONService.setup(with: config)
    }
    
    //MARK: 弹出一键登录授权页+用户点击登录后
    private func presentLocalLoginVC(){
        JVERIFICATIONService.getAuthorizationWith(self, hide: true, animated: true, timeout: 5*1000, completion: { (result) in
            if let result = result, let loginToken = result["loginToken"] as? String {
                
                
                JVERIFICATIONService.clearPreLoginCache()
                
                print("czjczjczj",loginToken)
//                self.getEncryptedPhoneNum(loginToken)
            }else{
                print("一键登录失败")
                self.otherLogin()
            }
        }) { (type, content) in
            if let content = content {
                print("一键登录 actionBlock :type = \(type), content = \(content)")
            }
        }
    }

}


//MARK: - 监听
extension UIViewController{
    @objc private func otherLogin(){
        JVERIFICATIONService.dismissLoginController(animated: true) {
            self.presentCodeLoginVC()
        }
    }
    
    @objc private func dismissLocalLoginVC(){
        JVERIFICATIONService.dismissLoginController(animated: true, completion: nil)
    }
}

//MARK: - 一般函数
extension UIViewController{
    func presentCodeLoginVC(){
        let mainSB =  UIStoryboard(name: "Main", bundle: nil)
        let loginNaviC = mainSB.instantiateViewController(identifier: kLoginNaviID)
        loginNaviC.modalPresentationStyle = .fullScreen
        present(loginNaviC, animated: true)
//        (presentedViewController as! UINavigationController).pushViewController(loginNaviC, animated: true)
    }
}

//MARK: - UI
extension UIViewController{
    private func setLocalLoginUI(){
        let config = JVUIConfig()
        config.prefersStatusBarHidden = true
        //导航栏
        config.navTransparent = true
        config.navText = NSAttributedString(string: " ")
        config.navReturnHidden = true
        config.navControl = UIBarButtonItem(title: "关闭", style: .plain, target: self, action: #selector(dismissLocalLoginVC))
        
        let constraintX = JVLayoutConstraint(attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, to: JVLayoutItem.super, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0)
        let logoConstraintY = JVLayoutConstraint(attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, to: JVLayoutItem.super, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1/7, constant: 0)
        config.logoConstraints = [logoConstraintY!,constraintX!]
        
        let  numberConstraintY = JVLayoutConstraint(attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, to: .super, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1, constant: 35)
        config.numberConstraints = [numberConstraintY!,constraintX!]
        
        let  sloganConstraintY = JVLayoutConstraint(attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, to: JVLayoutItem.number, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1, constant: 35)
        config.sloganConstraints = [sloganConstraintY!,constraintX!]
        
        config.logBtnText = "同意协议并一键登录"
        config.logBtnImgs = [
            UIImage(named: "localLoginBtn-nor")!,
            UIImage(named: "localLoginBtn-nor")!,
            UIImage(named: "localLoginBtn-hig")!
        ]
        
        let logBtnConstraintY = JVLayoutConstraint(attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, to: .slogan, attribute: .centerY, multiplier: 1, constant: 50)
        config.logBtnConstraints = [logBtnConstraintY!,constraintX!]
        
        config.privacyState = true
        config.checkViewHidden = true
        
        config.appPrivacyOne = ["用户协议","www.baidu.com"]
        config.appPrivacyTwo = ["隐私政策","www.baidu.com"]
        config.privacyComponents = ["登录注册代表你已同意","以及","和",""]
        config.appPrivacyColor = [UIColor.secondaryLabel,blueColor]
        config.privacyTextAlignment = .center
        
        let privacyContraintsW = JVLayoutConstraint(attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, to: JVLayoutItem.none, attribute: NSLayoutConstraint.Attribute.width, multiplier: 1, constant: 260)
        let privacyContraintsY = JVLayoutConstraint(attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, to: JVLayoutItem.super, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: -70)
        config.privacyConstraints = [privacyContraintsY!,constraintX!,privacyContraintsW!]
        
        config.agreementNavBackgroundColor = mainColor
        config.agreementNavReturnImage = UIImage(systemName: "chevron.left")
        
        JVERIFICATIONService.customUI(with: config){ customView in
            guard let customView = customView else { return }
            
            let otherLoginBtn = UIButton()
            otherLoginBtn.setTitle("其他方式登陆", for: .normal)
            otherLoginBtn.setTitleColor(.secondaryLabel, for: .normal)
            
            otherLoginBtn.translatesAutoresizingMaskIntoConstraints = false
            otherLoginBtn.addTarget(self, action: #selector(self.otherLogin), for: .touchUpInside)
            
            customView.addSubview(otherLoginBtn)
            
            NSLayoutConstraint.activate([
                otherLoginBtn.centerXAnchor.constraint(equalTo: customView.centerXAnchor),
                otherLoginBtn.centerYAnchor.constraint(equalTo: customView.centerYAnchor,constant: 170),
                otherLoginBtn.widthAnchor.constraint(equalToConstant: 279)
            ])
        }
        
    }
}


extension UIViewController{
    
    struct LocalLoginRes: Decodable{
        let phone: String
    }
    
    private func getEncryptedPhoneNum(_ loginToken:String){
        
        let headers: HTTPHeaders = [
            .authorization(username: kJAppKey, password: "99571df6af3d7e2ebfacf48a")
        ]
        
        let parameters = ["loginToken": loginToken]
        
        AF.request("https://api.verification.jpush.cn/v1/web/loginTokenVerify", method: .post, parameters: parameters, encoder: JSONParameterEncoder.default, headers: headers).responseDecodable(of: LocalLoginRes.self) { response in
            if let localLoginRes =  response.value{
                print(localLoginRes.phone)
            }
        }
        
    }
}

//
//  UIView-Extensions.swift
//  DanceTogether
//
//  Created by Zejun Chen on 2022/5/11.
//
import Foundation
import UIKit
import DateToolsSwift
import AVFoundation


extension UITextField{
    var unwrappedText: String {text ?? ""}
    var exactText: String {
        unwrappedText.isBlank ? "" : unwrappedText
    }
    var isBlank: Bool { unwrappedText.isBlank }
}

extension UITextView{
    var unwrappedText: String {text ?? ""}
    var exactText: String { unwrappedText.isBlank ? "" : unwrappedText }
    var isBlank: Bool { unwrappedText.isBlank }
}

extension UIView{
    @IBInspectable
    var radius: CGFloat{
        get{
            layer.cornerRadius
        }
        set{
            clipsToBounds = true
            self.layer.cornerRadius = newValue
        }
    }
}

//KVC-需要了解
extension UIAlertAction{
    func setTitleColor( _ color: UIColor){
        setValue(color, forKey: "titleTextColor")
    }
}

extension UILabel{
    func setToLight(_ text: String){
        self.text = text
        textColor = .label
    }
}

extension UIButton{
    
    func setToEnabled(){
        isEnabled = true
        backgroundColor = mainColor
    }
    
    func setToDisabled(){
        isEnabled = false
        backgroundColor = mainLightColor
    }
    
    func makeCapsule(_ color: UIColor = .label){
        layer.cornerRadius = frame.height / 2
        layer.borderWidth = 1
        layer.borderColor = color.cgColor
    }
}

extension UIImage{
    
    convenience init?(_ data: Data?) {
        if let unwrappedData = data{
            self.init(data: unwrappedData)
        }else{
            return nil
        }
    }
    
    enum JPEGQuality: CGFloat{
        case lowest = 0
        case low = 0.25
        case medium = 0.5
        case high = 0.75
        case highest = 1
    }
    func jpeg(_ jpegQuality: JPEGQuality) -> Data?{
        jpegData(compressionQuality: jpegQuality.rawValue)
    }
}

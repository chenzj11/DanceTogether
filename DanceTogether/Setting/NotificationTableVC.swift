//
//  NotificationTableVC.swift
//  DanceTogether
//
//  Created by Zejun Chen on 2022/5/10.
//

import UIKit

class NotificationTableVC: UITableViewController {
    
    var isNotDetermined = false
    @IBOutlet weak var toggleAllowNotificationSwitch: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUI()
        NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    @IBAction func toggleAllowNotification(_ sender: UISwitch) {
        if sender.isOn, isNotDetermined{
            
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { (granted, error) in
                if !granted{
                    self.setSwitch(false)
                }
            }
            isNotDetermined = false
        }else{
            jumpToSettings()
        }
    }
}

extension NotificationTableVC{
    private func setUI(){
        UNUserNotificationCenter.current().getNotificationSettings { settings in
            switch settings.authorizationStatus{
            case . notDetermined:
                self.setSwitch(false)
                self.isNotDetermined = true
            case .denied:
                self.setSwitch(false)
            default:
                self.setSwitch(true)
            }
        }
    }
    
    private func setSwitch(_ on: Bool){
        DispatchQueue.main.async {
            self.toggleAllowNotificationSwitch.setOn(on, animated: true)
        }
    }
    
    @objc private func willEnterForeground(){
        setUI()
    }
}
